// Assume that each salary amount is a factor of 10000 and correct it but
//  add it as a new key (corrected_salary or something)

const employ = require("./p1.js");
let result = employ.map(employObj => {
    let sal = employObj.salary.replace("$","");
    let newsal = parseFloat(sal)*10000;
    employObj['correctedSalary'] =  newsal;
    return employObj;
});

console.log(result);