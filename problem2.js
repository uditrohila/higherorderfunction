// 2. Convert all the salary values into proper numbers instead of strings
const employ = require("./p1.js");

let result = employ.map(employObj => {
    let value = employObj.salary.replace("$", "");
    value = parseFloat(value);
    return value;
});
console.log(result);
