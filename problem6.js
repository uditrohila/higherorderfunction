// Find the average salary of based on country using only HOF method

const employ = require("./p1");

const result = employ.reduce((acc,curr) => {
    if(!acc[curr.location]){
        acc[curr.location] = {"count":0, "totalSalary":0,"avgSalary":0}
    }
    
    
        acc[curr.location].count += 1;
        acc[curr.location].totalSalary += parseFloat(curr.salary.replace("$",""));
        acc[curr.location].avgSalary = (acc[curr.location].totalSalary) / (acc[curr.location].count);
    ``

    return acc;
},{})


let Arr = [];
for(let key in result){
    let avgSal = result[key].avgSalary;
    Arr.push([key,avgSal]);
}

console.log(Arr);


